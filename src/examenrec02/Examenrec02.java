/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenrec02;

/**
 *
 * @author Hector Ramirez
 */
public class Examenrec02 {
  
    private int numRecibo;
    private String nombre;
    private int puesto;
    private int nivel;
    private int dias;

    public Examenrec02() {
    }

    public Examenrec02(int numRecibo, String nombre, int puesto, int nivel, int dias) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.puesto = puesto;
        this.nivel = nivel;
        this.dias = dias;
    }
    
    public Examenrec02(Examenrec02 x) {
        this.numRecibo = x.numRecibo;
        this.nombre = x.nombre;
        this.puesto = x.puesto;
        this.nivel = x.nivel;
        this.dias = x.dias;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getDias() {
        return dias;
    }

    public void setDias(int dias) {
        this.dias = dias;
    }
    
    float calcularPago(){
        float pago = 0.0f;
        if (this.puesto == 1){
           pago = this.dias * 100;
        }
        if (this.puesto == 2){
           pago = this.dias * 200;
        }
        if (this.puesto == 3){
           pago = this.dias * 300;
        }
        return pago;
    }
    
    public float calcularImpuesto(){
        float impuesto = 0.0f;
        if (this.nivel == 1){
            impuesto = this.calcularPago() * 0.05f;
        }
        if (this.nivel == 2){
            impuesto = this.calcularPago() * 0.03f;
        }
        return impuesto;
    }
    public float calcularTotalPagar(){
        float total = 0.0f;
        total = this.calcularPago() - this.calcularImpuesto();
        return total;
    }
}
